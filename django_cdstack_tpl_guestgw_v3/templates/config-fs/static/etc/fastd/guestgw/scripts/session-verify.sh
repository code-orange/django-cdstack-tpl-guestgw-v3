#!/bin/bash

timestamp() {
  date +"%Y-%m-%d %H:%M:%S"
}

if grep -q "$PEER_KEY" /etc/fastd/guestgw/blacklist.dat; then
    echo "$(timestamp)\t$PEER_KEY\t$PEER_ADDRESS\tblocked" >> /var/log/guestgw_fastd_blacklist.log;
    exit 1;
else
    exit 0;
fi
