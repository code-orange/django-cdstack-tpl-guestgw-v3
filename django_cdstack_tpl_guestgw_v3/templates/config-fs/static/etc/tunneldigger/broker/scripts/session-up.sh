#!/bin/bash
INTERFACE="$3"

ip link set dev "$INTERFACE" up mtu 1364
batctl meshif bat0 if add "$INTERFACE"
