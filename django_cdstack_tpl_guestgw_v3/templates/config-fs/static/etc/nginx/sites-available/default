server {
    listen 80 default_server;
    listen [::]:80 default_server;
    listen 8080 default_server;
    listen [::]:8080 default_server;

    server_name _;

    location ^~ /.well-known/acme-challenge/ {
        access_log off;
        log_not_found off;

        proxy_pass {{ acme_forward_protocol }}acme;
        
        proxy_http_version 1.1;

        # Mitigating the HTTPoxy Vulnerability
        proxy_set_header Proxy "";

        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto $scheme;
    }

    location / {
        return 301 https://{{ node_hostname_fqdn }}$request_uri;
    }

}

server {
    listen 443 default_server ssl http2;
    listen [::]:443 default_server ssl http2;

    ssl_certificate /etc/ssl/certs/host.pem;
    ssl_certificate_key /etc/ssl/private/host.key;

    server_name _;

    location ^~ /.well-known/acme-challenge/ {
        access_log off;
        log_not_found off;

        proxy_pass {{ acme_forward_protocol }}acme;

        proxy_http_version 1.1;

        # Mitigating the HTTPoxy Vulnerability
        proxy_set_header Proxy "";

        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto $scheme;
    }

    location ~ \.json {
        root /var/www/html;
        add_header Access-Control-Allow-Origin *;
    }

    location /mrtg {
        alias /var/www/mrtg;
    }

    location / {
        include /etc/nginx/uwsgi_params;
        uwsgi_pass 127.0.0.1:9123;

        uwsgi_param Host $host;
        uwsgi_param HTTP_HOST {{ node_hostname_fqdn }};
        uwsgi_param X-Real-IP $remote_addr;
        uwsgi_param X-Forwarded-For $proxy_add_x_forwarded_for;
        uwsgi_param X-Forwarded-Proto $http_x_forwarded_proto;
    }
}
