import copy
import ipaddress

from django.template import engines

from django_cdstack_deploy.django_cdstack_deploy.func import generate_config_static
from django_cdstack_tpl_deb_bullseye.django_cdstack_tpl_deb_bullseye.views import (
    handle as handle_deb_bullseye,
)
from django_cdstack_tpl_deb.django_cdstack_tpl_deb.views import handle as handle_deb
from django_cdstack_tpl_deb.django_cdstack_tpl_deb.views import get_os_release
from django_cdstack_tpl_deb_stretch.django_cdstack_tpl_deb_stretch.views import (
    get_apt_packages,
    get_apt_sources,
    get_apt_prefs,
)
from django_cdstack_tpl_dnslb.django_cdstack_tpl_dnslb.views import (
    merge_dnsdist_resolver,
)
from django_cdstack_tpl_crowdsec_main.django_cdstack_tpl_crowdsec_main.views import (
    handle as handle_crowdsec_main,
)
from django_cdstack_tpl_fail2ban.django_cdstack_tpl_fail2ban.views import (
    handle as handle_fail2ban,
)
from django_cdstack_tpl_ipcop.django_cdstack_tpl_ipcop.views import (
    handle as handle_ipcop,
)
from django_cdstack_tpl_wazuh_agent.django_cdstack_tpl_wazuh_agent.views import (
    handle as handle_wazuh_agent,
)
from django_cdstack_tpl_whclient.django_cdstack_tpl_whclient.views import (
    handle as handle_whclient,
)
from django_cdstack_tpl_cloudrouter_v3.django_cdstack_tpl_cloudrouter_v3.views import (
    handle as handle_cloudrouter_v3,
)


def handle(zipfile_handler, template_opts, cmdb_host, skip_handle_os=False):
    django_engine = engines["django"]

    module_prefix = "django_cdstack_tpl_guestgw_v3/django_cdstack_tpl_guestgw_v3"

    if "apt_packages" not in template_opts:
        template_opts["apt_packages"] = get_apt_packages(
            module_prefix + "/templates/image/imageconf/vmtemplate.yml"
        )

    if "fail2ban_apps" not in template_opts:
        template_opts["fail2ban_apps"] = copy.deepcopy(template_opts["apt_packages"])

    if "apt_sources" not in template_opts:
        template_opts["apt_sources"] = get_apt_sources(
            module_prefix + "/templates/image/imageconf/vmtemplate.yml"
        )

    if "apt_preferences" not in template_opts:
        template_opts["apt_preferences"] = get_apt_prefs(
            module_prefix + "/templates/image/imageconf/vmtemplate.yml"
        )

    if "os_release" not in template_opts:
        template_opts["os_release"] = get_os_release(
            module_prefix + "/templates/image/imageconf/vmtemplate.yml"
        )

    dnsdist_resolver_list = merge_dnsdist_resolver(template_opts)

    template_opts = {**template_opts, **dnsdist_resolver_list}

    # IPv4 NAT
    guestgw_client_net_ip4 = ipaddress.ip_network(
        template_opts["network_iface_brclients_ip"]
        + "/"
        + template_opts["network_iface_brclients_netmask"],
        strict=False,
    )
    guestgw_client_net_ip6 = ipaddress.ip_network(
        template_opts["network_iface_brclients_ip6"]
        + "/"
        + template_opts["network_iface_brclients_netmask6"],
        strict=False,
    )

    template_opts["guestgw_client_net_cidr_ip4"] = str(guestgw_client_net_ip4)
    template_opts["guestgw_client_net_cidr_ip6"] = str(guestgw_client_net_ip6)

    # Custom IP Whiteliste
    template_opts["guestgw_network_access_white_list_ips"] = list()
    template_opts["guestgw_network_access_white_list_ip6s"] = list()

    for (
        guestgw_network_access_white_ip_key,
        guestgw_network_access_white_ip_val,
    ) in template_opts.items():
        if guestgw_network_access_white_ip_key.startswith(
            "guestgw_network_access_white_list_ip_"
        ):
            template_opts["guestgw_network_access_white_list_ips"].append(
                str(
                    ipaddress.ip_network(
                        guestgw_network_access_white_ip_val, strict=False
                    )
                )
            )

    for (
        guestgw_network_access_white_ip6_key,
        guestgw_network_access_white_ip6_val,
    ) in template_opts.items():
        if guestgw_network_access_white_ip6_key.startswith(
            "guestgw_network_access_white_list_ip6_"
        ):
            template_opts["guestgw_network_access_white_list_ip6s"].append(
                str(
                    ipaddress.ip_network(
                        guestgw_network_access_white_ip6_val, strict=False
                    )
                )
            )

    generate_config_static(zipfile_handler, template_opts, module_prefix)
    handle_cloudrouter_v3(zipfile_handler, template_opts, cmdb_host, skip_handle_os)

    return True
